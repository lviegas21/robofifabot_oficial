import requests
from bs4 import BeautifulSoup
from datetime import datetime
import time
def last_chat_id(token):
    try:
        url = "https://api.telegram.org/bot{}/getUpdates".format(token)
        response = requests.get(url)
        if response.status_code == 200:
            json_msg = response.json()
            print(json_msg)
            for json_result in reversed(json_msg['result']):
                message_keys = json_result['message'].keys()
                if ('new_chat_member' in message_keys) or ('group_chat_created' in message_keys):
                    return json_result['message']['chat']['id']
            print('Nenhum grupo encontrado')
        else:
            print('A resposta falhou, código de status: {}'.format(response.status_code))
    except Exception as e:
        print("Erro no getUpdates:", e)

# enviar mensagens utilizando o bot para um chat específico
def send_message(token, chat_id, message,  parse_mode = 'Markdown'):
    print("Id do chat:", chat_id)
    time_right = []
    time_left = []
    link_scrapping = []
    url_site = 'https://www.totalcorner.com/league/view/12995'

    response = requests.get(url_site)
    html = BeautifulSoup(response.text, 'html.parser')

    for jogos in html.select('tr'):
        hora = jogos.select_one('td')
        time_r = jogos.select_one('.text-right a')
        time_l = jogos.select_one('.text-left a')
        if time_r == None:
            print('')
        if time_l == None:
            print('')
        else:
            time_right.append(time_r.string)
            time_left.append(time_l.string)

    url2 = 'https://www.totalcorner.com/'
    new_scrapping = ''
    response = requests.get(url_site)
    html = BeautifulSoup(response.text, 'html.parser')


    for links in html.select('tr'):
        link_partida = links.select_one('.text-center a')
        if link_partida != None:
            url_final = f'{link_partida["href"]}'
            new_scrapping = url2 + url_final[0:30]
            link_scrapping.append(new_scrapping)
    print('len do scrapping: ', len(link_scrapping))
    cont = 0
    print(len(time_left))
    print(len(time_right))
    for c in range(len(link_scrapping)):
        print('loop: ', c)
        print(link_scrapping[c])
        response_n = requests.get(link_scrapping[c])
        html = BeautifulSoup(response_n.text, 'html.parser')
        variavel = '0'
        score_c = 0
        score_f = 0
        dang_c = 0
        dang_f = 0
        #requests.exceptions.ConnectionError: ('Connection aborted.', ConnectionResetError(10054, 'Foi forçado o cancelamento de uma conexão existente pelo host remoto', None, 10054, None))
        for status in html.select('p'):
            var = status.select_one('.red')
            if var != None:
                variavel = var.text
                break
        value = variavel.replace(' ', '')
        c = 0
        for status in html.select('.panel-body p'):
            score = status.select_one('span')
            if score != None:
                if c == 0:

                    if 'full' in value:
                        score_c = status.text[23:26].replace(' ', '')
                        score_f = status.text[27:29].replace(' ', '')
                        break

                    elif len(value) > 0:
                        if len(value) < 3:
                            score_c = '0' + status.text[24: 25].replace('\n', '')
                            score_f = '0' + status.text[28: 29].replace('\n', '')
                            break
                        else:
                            score_c = status.text[24: 25].replace(' ', '')
                            score_f = status.text[27: 29].replace(' ', '')
                            break
                    else:
                        score_c = status.text[20:22].replace(' ', '')
                        score_f = status.text[24:25].replace(' ', '')
                        break

            c = c + 1
        placar_d = score_c
        atta = 0
        attack_l = 0
        for status in html.select('.row'):
            dangs_c = status.select_one('.text-right')
            dangs_f = status.select_one('.text-left')

            # jogo full
            if dangs_c != None and dangs_f != None:
                if len(value) < 4:
                    if atta == 8 and attack_l == 8:
                        dang_c = dangs_c.text
                        dang_f = dangs_f.text
                        print('a',dang_c, dang_f)
                        break

                else:
                    if atta == 8 and attack_l == 8:
                        if len(dangs_c.text) > 3 and len(dangs_f.text) > 3:
                            dang_c = 0
                            dang_f = 0

                            break
                        else:
                            dang_c = dangs_c.text
                            dang_f = dangs_f.text
                            print('b', dang_c, dang_f)
                            break



                atta = atta + 1
                attack_l = attack_l + 1
        print(time_right[cont], {score_c}, 'X', {score_f} , time_left[cont])
        print('attacks casa: ', dang_c, 'attacks fora: ', dang_f)
        print('score time da casa: ', score_c, 'score time de fora: ', score_f)



        negrito = '*'
        url3 = 'https://www.bet365.com/#/IP/AC/B1/C1/D13/E47578772/F2/'

        # if valor4 > 0:
        #     dang_c = dang_cs
        #     dang_f = dang_fo
        print('Esse é o status do jogo: ', value)
        if value == 'full':
            print('value 1', value)
        elif value == '':
            value = 0
            print('value 2: ', value)
        else:
            if value == 'half':
                print('valor em half')
                value = '04'
            valor = value.replace("'", "")
            if int(valor) == 3:
                print('value 3: ', valor)
                print('tempo passou')
                if int(dang_c) == 0:
                    dang_c = 1
                if int(dang_f) == 0:
                    dang_f = 1
                if int(score_c) == int(score_f):
                    if int(dang_c) >= 5 or int(dang_f) >= 5:
                        print('condições foram aceitas')
                        if (int(dang_c) * 0.5) >= int(dang_f):
                            print(dang_c, dang_f)
                            print('mensagem vai ser enviada')
                            try:
                                message = f'🚨 {negrito}ALERTA{negrito} | 🤖 {negrito}ROBÔ FIFÃO{negrito} \n' \
                                          f'\n ⚽ {negrito}Jogo:{negrito} {time_right[cont]} {score_c} x {score_f} {time_left[cont]}\n'\
                                          f'\n ✅ {negrito}Entrada{negrito}: Handicap 0.0 a favor do {time_left[cont]}\n' \
                                          f'\n ⭐ {negrito}Sugestão:{negrito} Entrada com 1% do capital\n' \
                                          f'\n {url3}'

                                data = {"chat_id": chat_id, "text": message, 'parse_mode': 'Markdown'}
                                url = "https://api.telegram.org/bot{}/sendMessage".format(token)
                                requests.post(url, data)
                                time.sleep(20)
                            except Exception as e:
                                print("Erro no sendMessage:", e)
                        elif (int(dang_f) * 0.5) >= int(dang_c):
                            print(dang_c, dang_f)
                            print('mensagem vai ser enviada')
                            try:
                                print('oi')
                                message = f'🚨 {negrito}ALERTA{negrito} | 🤖 {negrito}ROBÔ FIFÃO{negrito} \n' \
                                          f'\n ⚽ {negrito}Jogo:{negrito} {time_right[cont]} {score_c} x {score_f} {time_left[cont]}\n'\
                                          f'\n ✅ {negrito}Entrada{negrito}: Handicap 0.0 a favor do {time_right[cont]}\n' \
                                          f'\n ⭐ {negrito}Sugestão:{negrito} Entrada com 1% do capital\n' \
                                          f'\n {url3}'

                                data = {"chat_id": chat_id, "text": message, 'parse_mode': 'Markdown'}
                                url = "https://api.telegram.org/bot{}/sendMessage".format(token)
                                requests.post(url, data)
                                time.sleep(20)
                            except Exception as e:
                                print("Erro no sendMessage:", e)
        time.sleep(0.6)
        cont+=1
        if cont == len(link_scrapping):
            cont = 0


token = '1639022820:AAGLR6gcycB_S_S73VZtMrOK0farhix0efo'

while True:
    # id do chat que será enviado as mensagens
    '-1001261537682'
    'last_chat_id(token)'
    chat_id = '-1001261537682'
    message = ''

    # enviar a mensagem
    send_message(token, chat_id, message)
